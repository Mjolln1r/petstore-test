import os

import requests

petId = 1
url = f"https://petstore.swagger.io/v2/pet/{petId}/uploadImage"
image_path = './tests/pet/images/Screenshot from 2024-03-20 18-04-49.png'
min_size_image_path = './tests/pet/images/min size image.png'
headers = {'Content-Type': 'multipart/form-data'}


def test_status_code_200_by_correct_image():
    response = requests.post(url, files={'file': open(image_path, 'rb')})
    assert response.status_code == 200, "Image not send"


def test_status_code_415_by_send_none():
    response = requests.post(url)
    assert response.status_code == 415, "None sent"


def test_send_img_min_size_format_jpeg():
    response = requests.post(url, files={'file': open(min_size_image_path, 'rb')})
    assert response.status_code == 200, "Image format .jpeg min size not sending"


def test_send_img_max_size_format_jpeg():
    response = requests.post(url, files={'file': open(image_path, 'rb')})
    assert response.status_code == 200, "Image format .jpeg max size not sending"


def test_send_img_exhaustive_size_format_jpeg():
    response = requests.post(url, files={'file': open(image_path, 'rb')})
    assert response.status_code == 200, "Expected status 415"


def test_send_incorrect_format_pet_id():
    response = requests.post('https://petstore.swagger.io/v2/pet/Emile/uploadImage',
                             files={'file': open(image_path, 'rb')})
    assert response.status_code == 404, "Incorrect id format"
