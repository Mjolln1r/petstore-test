import json

from pydantic import BaseModel


class Car:
    id: int
    name: str
    age: int

    def __init__(self, id: int, name: str, age: int):
        self.id = id
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.id}: {self.name} {self.age}"


new_car = Car(id=1, name='Car', age=18)

print(json.dumps(new_car.__dict__))
