import pytest
import requests
from pydantic import ValidationError

from configuration import BASE_URL, HEADERS
from src.models.pet.pet import Pet
from src.schemas.pet import pet_json


def test_add_new_pet():
    Pet.parse_raw(requests.post(f'{BASE_URL}/pet', json=pet_json, headers=HEADERS).text)
