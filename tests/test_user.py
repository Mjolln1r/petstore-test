import requests
from src.models.user.user import User
from src.schemas.user import user_json
from configuration import BASE_URL, HEADERS
from pydantic import ValidationError


def test_create_user():
    try:
        User.parse_raw(requests.post(f'{BASE_URL}/user/createWithArray', json=user_json, headers=HEADERS).text)
    except ValidationError as err:
        print('Exception', err)
