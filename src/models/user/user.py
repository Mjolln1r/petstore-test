from pydantic import BaseModel, constr, EmailStr, ValidationError, conlist
from src.schemas.user import user_json
from src.models.user.userlist import UserList


class User(BaseModel):
    code: int
    type: str
    message: str
