from pydantic import constr, EmailStr, ValidationError, BaseModel


class UserList(BaseModel):
    id: int
    username: constr(min_length=3, max_length=50)
    firstName: constr(min_length=3, max_length=50)
    lastName: constr(min_length=3, max_length=50)
    email: str
    password: str
    phone: str
    userStatus: int
