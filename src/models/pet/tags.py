from pydantic import BaseModel, constr


class Tag(BaseModel):
    id: int
    name: constr(min_length=3, max_length=20)
