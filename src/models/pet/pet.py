from pydantic import BaseModel, field_validator, ValidationError, constr, conint
from src.models.pet.category import Category
from src.models.pet.tags import Tag
import re


class Pet(BaseModel):
    id: conint(ge=0)
    category: Category
    name: constr(min_length=3, max_length=50)
    photoUrls: list[str]
    tags: list[Tag]
    status: str

    @field_validator('status')
    def status_must_be_valid(cls, value: str):
        pattern = r'^(available|pending|sold)$'
        if not re.match(pattern, value):
            raise ValidationError('status must be one of: available, pending, sold')
        return value

    @staticmethod
    def fabric(id: int = 1, category: Category = Category(id=1, name="Pet"), name: str = "test1",
               photoUrls: list[str] = ["photo1"], tags: list[Tag] = [Tag(id=1, name="test"), Tag(id=2, name="test2")],
               status: str = "sold"):
        return Pet(id=id, category=category, name=name, photoUrls=photoUrls, tags=tags, status=status).model_dump_json()

    @staticmethod
    def photoUrlsGenerator():
        # request
        return ['photo.jpg', "photo2.jpg"]


# print(Pet(id=1, category=Category(id=1, name="Pet"), name="peti", photoUrls=["string"], tags=[Tag(id=1, name="Tags")],
#     status="sold").model_dump_json())



print(Pet.fabric(id=2, photoUrls=Pet.photoUrlsGenerator()))
