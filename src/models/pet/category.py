from pydantic import BaseModel
from pydantic import constr


class Category(BaseModel):
    id: int
    name: constr(min_length=3, max_length=50)
